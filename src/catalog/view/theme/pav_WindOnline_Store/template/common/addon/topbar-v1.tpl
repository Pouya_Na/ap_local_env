<nav id="topbar" class="topbar-v1">
	<div class="container">
		<div class="inside">
			<div class="row">
				<div class="pull-left">
					<?php echo $currency; ?>
					<?php echo $language; ?>				
				</div>		
				<div class="pull-right">
					<ul class="text-phone pull-left hidden-sm hidden-xs">
						<?php
							if($content=$helper->getLangConfig('widget_about')){
								echo $content;
							}
						?>
					</ul>
					<ul class="login list-inline pull-left">
						<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu dropdown-menu-right">
								
 <?php if ($logged) { ?>
			<?php if ($ms_seller_created) { ?>
			<?php if ($this->MsLoader->MsSeller->getStatus($this->customer->getId()) == MsSeller::STATUS_ACTIVE) { ?>
			<li><a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>"><?php echo $ms_account_dashboard; ?></a></li>
			<?php } ?>

			<li><a href= "<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>"><?php echo $ms_account_sellerinfo; ?></a></li>
			<?php if ($this->MsLoader->MsSeller->getStatus($this->customer->getId()) == MsSeller::STATUS_ACTIVE) { ?>
			<li><a href= "<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>"><?php echo $ms_account_newproduct; ?></a></li>
			<li><a href= "<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>"><?php echo $ms_account_products; ?></a></li>
			<li><a href= "<?php echo $this->url->link('seller/account-order', '', 'SSL'); ?>"><?php echo $ms_account_orders; ?></a></li>
			<li><a href= "<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>"><?php echo $ms_account_transactions; ?></a></li>

			<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
			<li><a href= "<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>"><?php echo $ms_account_withdraw; ?></a></li>
			<?php } ?>

			<li><a href= "<?php echo $this->url->link('seller/account-stats', '', 'SSL'); ?>"><?php echo $ms_account_stats; ?></a></li>
			<li><a href="<?php echo $this->url->link('account/logout', '', 'SSL'); ?>"><?php echo $text_logout; ?></a></li>
			<?php } ?>
			<?php } else { ?>
			<li><a href="<?php echo $this->url->link('account/register-seller', '', 'SSL'); ?>"><?php echo $ms_account_register_seller; ?></a></li>
			<li><a href="<?php echo $this->url->link('account/login', '', 'SSL'); ?>"><?php echo $text_login; ?></a></li>
			<?php } ?>
            <?php } else { ?>
            <li>  <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li>  <a href="<?php echo $login; ?>"><?php echo $text_login; ?></a>  </li>
            <?php } ?>								
							</ul>
						</li>
					</ul>
					<ul class="list-inline pull-left">
						<li><a id="wishlist-total" href="<?php echo $wishlist; ?>"><i class="hidden-lg hidden-md fa-fw fa fa-list-alt"></i><span class="hidden-xs"><?php echo $text_wishlist; ?></span></a></li>
						<li><a href="<?php echo $shopping_cart; ?>"><i class="hidden-lg hidden-md fa-fw fa fa-shopping-cart"></i><span class="hidden-xs"><?php echo $text_shopping_cart; ?></span></a></li>
						<li><a href="<?php echo $checkout; ?>"><i class="hidden-lg hidden-md fa-fw fa fa-share"></i><span class="hidden-xs"><?php echo $text_checkout; ?></span></a></li>
					</ul>
				</div>
			</div>
			<!-- end row -->
		</div>
	</div>
</nav>