<div class="product-filter space-20 clearfix">
	<div class="display col-lg-5 col-md-3 col-sm-3 hidden-xs">
		<div class="btn-group group-switch">
			<button type="button" id="list-view" class="btn btn-switch" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
			<button type="button" id="grid-view" class="btn btn-switch active" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
			<div class="product-compare pull-left">
				<a href="<?php echo $compare; ?>" class="btn-link" data-toggle="tooltip" title="Compare this Product">
					<i class="fa fa-exchange"></i> <span id="compare-total"><?php echo $text_compare; ?></span>
				</a>
			</div>
		</div>
	</div>
	<div class="filter-right clearfix col-lg-7 col-md-9 col-sm-9 col-xs-12">
		<div class="limit pull-right col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="gr-text pull-left">
				<span for="input-limit"><?php echo $text_limit; ?></span>
			</div>
			<div class="gr-select pull-left">
				<select id="input-limit" class="form-control" onchange="location = this.value;">
					<?php foreach ($limits as $limits) { ?>
					<?php if ($limits['value'] == $limit) { ?>
					<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div> 
		</div> 
		<div class="sort pull-right col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="gr-text pull-left">
				<span for="input-sort"><?php echo $text_sort; ?></span>
			</div>
			<div class="gr-select pull-left">
				<select id="input-sort" class="form-control" onchange="location = this.value;">
					<?php foreach ($sorts as $sorts) { ?>
					<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
					<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
</div>