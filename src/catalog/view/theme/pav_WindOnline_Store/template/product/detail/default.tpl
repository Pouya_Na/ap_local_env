<?php 
    $mode = 'horizontal';
    $cols = array( 'default' => array(5,7),
                   'horizontal' => array(5,7)
    ); 
    $cols = $cols[$mode];    
    $objlang = $this->registry->get('language'); 
    $button_compare = $objlang->get("Compare"); 
    $button_wishlist = $objlang->get("Wishlist");
?>

<div class="product-info bg-white">
    <div class="row">
        <div class="product-info-bg">
            <?php require( ThemeControlHelper::getLayoutPath( 'common/detail/'.$mode.'.tpl' ) );  ?>   
            <div class="product-view col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
				<!--  <div class="row"> -->
                    <!-- <div class="col-lg-7 space-padding-lr-40"> -->
                        <!-- <h1 class="title-product"><?php //echo $heading_title; ?></h1> -->
                          <div class="clearfix">  
							<div class="compare pull-left">
                                <button type="button" data-toggle="tooltip" class="btn btn-outline border-2" title="<?php echo $button_compare; ?>" onclick="compare.addcompare('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
                            </div>							
                            <div class="wishlist pull-left">  
                                <button type="button" data-toggle="tooltip" class="btn btn-outline border-2" title="<?php echo $button_wishlist; ?>" onclick="wishlist.addwishlist('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
                            </div>					
							<?php if ($review_status) { ?>
								<div class="rating pull-right">
									<?php for ($i = 1; $i <= 5; $i++) { ?>
										<?php if ($rating < $i) { ?>
											<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										<?php } else { ?>
											<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
										<?php } ?>
									<?php } ?>
									<p><a href="#review-form"  class="popup-with-form" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><?php echo $text_write; ?></a> - <a href="#review-form" class="popup-with-form" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><?php echo $reviews; ?></a></p>
								</div>
							<?php } ?>
						</div>
						<h1 class="name"><?php echo $heading_title; ?></h1>
                        <div class="border-success">
                            <ul class="list-unstyled">
                                <?php if ($manufacturer) { ?>
                                    <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
                                <?php } ?>
                                <li><?php echo $text_model; ?> <?php echo $model; ?></li>
                                <?php if ($reward) { ?>
                                    <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
                                <?php } ?>
                                <?php if ($points) { ?>
                                    <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                                <?php } ?>
                            </ul>
							<?php if ($tags) { ?>
							<p><?php echo "<b>".$text_tags."</b>"; ?>
								<?php for ($i = 0; $i < count($tags); $i++) { ?>
								<?php if ($i < (count($tags) - 1)) { ?>
								<a href="<?php echo $tags[$i]['href']; ?>"><?php echo trim($tags[$i]['tag']); ?></a>,
								<?php } else { ?>
								<a href="<?php echo $tags[$i]['href']; ?>"><?php echo trim($tags[$i]['tag']); ?></a>
								<?php } ?>
								<?php } ?>
							</p>
							<?php } ?>
                        </div>
                        <?php if ($stock) { ?>
						<ul class="list-unstyled">
                            <li><span class="check-box text-success"></span> <?php echo $text_stock; ?><?php echo $stock; ?></li>
                        </ul>
                        <?php } ?>						
                        <?php if ($price) { ?>
                            <div class="price detail">
                                <ul class="list-unstyled">
                                    <?php if (!$special) { ?>
                                        <li><span class="price-new"> <?php echo $price; ?> </span></li>
                                    <?php } else { ?>
                                        <li> <span class="price-new"> <?php echo $special; ?> </span> <br><span class="price-old"><?php echo $price; ?></span></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                        <!-- <ul class="list-unstyled tax">
                            <?php if ($tax) { ?>
                                <li><?php //echo $text_tax; ?> <?php //echo $tax; ?></li>
                            <?php } ?>
                            <?php if ($discounts) { ?>
                                <li></li>
								<?php foreach ($discounts as $discount) { ?>
									<li><?php echo $discount['quantity']; ?><?php //echo $text_discount; ?><?php echo $discount['price']; ?></li>
								<?php } ?>
                            <?php } ?>
                        </ul>		 -->				

                        <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 
                        <!-- AddThis Button END -->
                    <!-- </div> -->
                    <div id="product">
                        <?php if ($options) { ?>
                            <h3><?php echo $text_option; ?></h3>
                            <?php foreach ($options as $option) { ?>
                                <?php if ($option['type'] == 'select') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
										<div class="gr-select">
											<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
												<option value=""><?php echo $text_select; ?></option>
												<?php foreach ($option['product_option_value'] as $option_value) { ?>
													<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
														<?php if ($option_value['price']) { ?>
															(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
														<?php } ?>
													</option>
												<?php } ?>
											</select>
										</div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'radio') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'checkbox') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'image') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'text') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'textarea') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'file') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default upload"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'date') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> group-date">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group date">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            </div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<span class="input-group-btn">
													<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i> Date</button>
												</span>
											</div>	
										</div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'datetime') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> group-datetime">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group datetime">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            </div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i> Date time</button>
												</span>
											</div>
										</div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'time') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group time">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span></div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <?php if ($recurrings) { ?>
                            <hr>
                            <h3><?php echo $text_payment_recurring ?></h3>
                            <div class="form-group required">
                                <select name="recurring_id" class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($recurrings as $recurring) { ?>
                                        <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block" id="recurring-description"></div>
                            </div>
                        <?php } ?>
                        <div class="product-meta clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="quantity clearfix">
									<label class="control-label qty col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $entry_qty; ?>.</label>
									<div class="quantity-adder col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 quantity-number">
											<input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 gr-add-action">
											<span class="add-up add-action"><i class="fa fa-caret-up"></i></span>
											<span class="add-down add-action"><i class="fa fa-caret-down"></i></span>
										</div>
									</div>
								</div>
                            </div>

                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
							
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="cart">
									<button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-inverse btn-primary"><i class="fa-fw fa fa-shopping-cart"></i><?php echo $button_cart; ?></button>
								</div> 
							</div>                                                        
                        </div>
                        <div class="clearfix"></div>
                            <?php if ($minimum > 1) { ?>
                                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                            <?php } ?> 
                    </div> <!-- end product --> 
                    <?php if (isset($seller) && !empty($seller)) { ?>
                    <hr />
                    <div class="ms-sellerprofile description">
                        <h3><?php echo $ms_catalog_product_sellerinfo; ?></h3>
                        <div class="seller-data">
                            <div class="avatar-box">
                                <a href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" /></a>
                            </div>
                            <div class="info-box">
                            <ul class="list-unstyled">
                                <li><a class="nickname" style="text-decoration: none" href="<?php echo $seller['href']; ?>"><b><?php echo $seller['nickname']; ?></b></a></li>
                                <?php if ($seller['country']) { ?>
                                    <li><?php echo $ms_catalog_seller_profile_country; ?></b> <?php echo $seller['country']; ?></li>
                                <?php } ?>

                                <?php if ($seller['zone']) { ?>
                                    <li><?php echo $ms_catalog_seller_profile_zone; ?></b> <?php echo $seller['zone']; ?></li>
                                <?php } ?>

                                <?php if ($seller['company']) { ?>
                                    <li><?php echo $ms_catalog_seller_profile_company; ?></b> <?php echo $seller['company']; ?></li>
                                <?php } ?>

                                <?php if ($seller['website']) { ?>
                                    <li><?php echo $ms_catalog_seller_profile_website; ?></b> <?php echo $seller['website']; ?></li>
                                <?php } ?>

                                <li><?php echo $ms_catalog_seller_profile_totalsales; ?></b> <?php echo $seller['total_sales']; ?></li>
                                <li><?php echo $ms_catalog_seller_profile_totalproducts; ?></b> <?php echo $seller['total_products']; ?></li>
                                <?php if ($this->config->get('mmess_conf_enable') || $this->config->get('msconf_enable_private_messaging') == 2) { ?>
                                    <?php if ((!$this->customer->getId()) || ($this->customer->getId() != $seller['seller_id'])) { ?>
                                        <?php echo $contactForm; ?>
                                        <div class="contact">
                                            <?php if ($this->customer->getId()) { ?>
                                              <div class="button-group">
                                                <button type="button" class="btn btn-default btn-block ms-sellercontact" data-toggle="modal" data-target="#contactDialog"><span><?php echo $ms_catalog_product_contact; ?></span></button>
                                              </div>
                                            <?php } else { ?>
                                                <?php echo sprintf($this->language->get('ms_sellercontact_signin'), $this->url->link('account/login', '', 'SSL'), $seller['nickname']); ?>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                            </div>
                        </div>
                        <!-- end sellerinfo -->
                    </div>
                    <hr />
                    <!-- end seller block -->
                    <?php } ?>                           
                <!-- </div> end row -->                 
            </div><!--  end product-view -->
        </div><!-- End div product-info-bg -->
    </div> <!-- end row -->   
</div> <!-- end product-info -->

<div class="clearfix box-product-infomation tab-v7 bg-white  space-top-50">
    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-4 tab-heading text-center">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#tab-description" data-toggle="tab"><i class="fa fa-file-text"></i><br><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?>
                <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <?php if( $productConfig['enable_product_customtab'] && isset($productConfig['product_customtab_name'][$languageID]) ) { ?>
                <li><a href="#tab-customtab" data-toggle="tab"><i class="fa fa-cog"></i><br><?php echo $productConfig['product_customtab_name'][$languageID]; ?><span class="triangle-bottomright"></span></a></li>
            <?php } ?>			
            <?php if ($review_status) { ?>
                <li><a href="#tab-review" data-toggle="tab"><i class="fa fa-comments"></i><br><?php echo $tab_review; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="col-lg-11 col-md-11 col-sm-10 col-xs-8 tab-content width-80">
        <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
        <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
                <table class="table table-bordered">
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <thead>
                            <tr>
                                <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                <tr>
                                    <td><?php echo $attribute['name']; ?></td>
                                    <td><?php echo $attribute['text']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>
        <?php if ($review_status) { ?>

            <div class="tab-pane" id="tab-review">

                <div id="review" class="space-20"></div>
                <p> <a href="#review-form"  class="popup-with-form btn btn-sm btn-danger" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ><?php echo $text_write; ?></a></p>

               <div class="hide"> <div id="review-form" class="panel review-form-width"><div class="panel-body">
                <form class="form-horizontal ">
                 
                    <h2><?php echo $text_write; ?></h2>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                            <input type="text" name="name" value="" id="input-name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                            <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                            <div class="help-block"><?php echo $text_note; ?></div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label"><?php echo $entry_rating; ?></label>
                            &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                            <input type="radio" name="rating" value="1" />
                            &nbsp;
                            <input type="radio" name="rating" value="2" />
                            &nbsp;
                            <input type="radio" name="rating" value="3" />
                            &nbsp;
                            <input type="radio" name="rating" value="4" />
                            &nbsp;
                            <input type="radio" name="rating" value="5" />
                            &nbsp;<?php echo $entry_good; ?></div>
                    </div>
                    <?php if ($site_key) { ?>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                    </div>
                  </div>
                <?php } ?>
                    <div class="buttons">
                        <div class="pull-right">
                            <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                        </div>
                    </div>
                </form></div></div></div>

            </div>
        <?php } ?>
        <?php if( $productConfig['enable_product_customtab'] && isset($productConfig['product_customtab_content'][$languageID]) ) { ?>
            <div id="tab-customtab" class="tab-content tab-pane custom-tab">
                <div class="inner">
                    <?php echo html_entity_decode( $productConfig['product_customtab_content'][$languageID], ENT_QUOTES, 'UTF-8'); ?>
                </div>
            </div>
        <?php } ?>        
    </div>
</div>
<?php if ($products) {  $heading_title = $text_related; $customcols = 4; ?>
<div class="panel panel-default panel-v1 product-grid related bg-white space-top-50"> 
<?php require( PAVO_THEME_DIR."/template/common/products_owl_module.tpl" );  ?>
</div>
<?php } ?>
